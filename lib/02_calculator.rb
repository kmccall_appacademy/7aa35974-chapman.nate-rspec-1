def add(num1, num2)
  return num1 + num2
end

def subtract(num1, num2)
  return num1 - num2
end

def sum(num_array)
  num_array.empty? ? 0 : num_array.reduce(:+)
end
