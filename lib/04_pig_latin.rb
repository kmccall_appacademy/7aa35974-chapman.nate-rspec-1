def latinize(word)
  vowels = 'aeiou'.chars

  case
  when word[0..1] == 'qu'
    return word[2..-1] + 'quay'
  when word[0..2] == 'sch'
    return word[3..-1] + 'schay'
  when vowels.include?(word[0])
    return word + 'ay'
  else
    if vowels.include?(word[1])
      return word[1..-1] + word[0] + 'ay'
    elsif vowels.include?(word[2])
      return word[2..-1] + word[0..1] + 'ay'
    elsif vowels.include?(word[3])
      return word[3..-1] + word[0..2] + 'ay'
    end
  end

end

def translate(phrase)
  phrase.split.map { |word| latinize(word)}.join(" ")

end
