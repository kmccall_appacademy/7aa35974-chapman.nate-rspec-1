require 'byebug'

def echo(str)
  return str
end

def shout(str)
  str.downcase.upcase
end

def repeat(str, number_of_repeats = 2)
  str + " #{str}" * (number_of_repeats - 1)

end

def start_of_word(str, num_letters)
  str[0, num_letters]

end

def first_word(phrase)
  phrase.split(" ")[0]
end

def titleize(phrase)

  little_words = ['the', 'a', 'and', 'over', 'oh']
  phrase.split(" ").map.with_index { |word, idx|
    if little_words.include?(word)
      if idx == 0
        word.capitalize
      else
        word
      end

    else
      word.capitalize
    end
  }.join(" ")
end
